\documentclass{beamer}

\usepackage[backend=bibtex,maxnames=100]{biblatex} %from texlive-bibtex-extra
\renewcommand{\footnotesize}{\tiny}

\usepackage{listings}
\usepackage{upquote} % proper backticks in listings
\usepackage{xcolor}
\definecolor{verylightgray}{gray}{0.95}
\definecolor{somewhatdarkgray}{gray}{0.3}
\lstset{
  language=,
  basicstyle=\small\ttfamily,
  numberstyle=\tiny,
  numbersep=5pt,
  tabsize=2,
  extendedchars=true,
  breaklines=true,
  keywordstyle=\color{red},
  stringstyle=\color{blue}\ttfamily,
  numberstyle=\color{violet},
  showspaces=false,
  showtabs=false,
  showstringspaces=false,
  backgroundcolor=\color{verylightgray},
  frame=single,
  framexleftmargin= 3px,
  framexrightmargin= 3px,
  rulecolor=\color{lightgray}
}

\usepackage{booktabs}

\usetheme{Madrid}
\setbeamertemplate{navigation symbols}{} % Remove the navigation symbols
\setbeamertemplate{sections/subsections in toc}[default] %Turn off ugly toc numbering
\setbeamertemplate{itemize subitem}[triangle]
\setbeamertemplate{itemize item}[triangle]
\setbeamertemplate{enumerate items}[default]
\setbeamercovered{transparent}
\usepackage{appendixnumberbeamer}
\defbeamertemplate{section page}{customsection}[1][]{%
  \begin{centering}
    {\usebeamerfont{section name}\usebeamercolor[fg]{section name}#1}
    \vskip1em\par
    \begin{beamercolorbox}[sep=12pt,center]{part title}
      \usebeamerfont{section title}\insertsection\par
    \end{beamercolorbox}
  \end{centering}
}
\defbeamertemplate{subsection page}{customsubsection}[1][]{%
  \begin{centering}
    {\usebeamerfont{subsection name}\usebeamercolor[fg]{subsection name}#1}
    \vskip1em\par
    \begin{beamercolorbox}[sep=8pt,center,#1]{part title}
      \usebeamerfont{subsection title}\insertsubsection\par
    \end{beamercolorbox}
  \end{centering}
}
\AtBeginSection{\frame{\sectionpage}}
\AtBeginSubsection{\frame{\subsectionpage}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\graphicspath{{./images/}}

% Customize hyperref here
% color links, but not internal links (confusing..)
\hypersetup{colorlinks=true,linkcolor=,urlcolor=magenta}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\author[Patrick Sanan]{Patrick Sanan}
\institute{ETH Zurich}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title[Sphinx for PETSc]{Sphinx Documentation for PETSc/TAO}
\date{October 1, 2020}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\setbeamertemplate{section page}[customsection]
\setbeamertemplate{subsection page}[customsubsection]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
\titlepage
\begin{center}
\texttt{Thanks-to:} too many people involved in PETSc to list individually!
\vspace{20px}
\url{https://gitlab.com/psanan/petsc-sphinx-slides}
\end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{Objectives for this Tutorial}
  \begin{enumerate}
    \item Motivate improving the documentation
    \item Give an introduction to Sphinx and how it works
    \item Discuss how it's currently used with PETSc
    \item Describe how to contribute
    \item Discuss possible future improvements
  \end{enumerate}
  \vspace{20px}
  These slides include many clickable links (and try to avoid reiterating information available there) and can be downloaded from \url{https://gitlab.com/psanan/petsc-sphinx-slides/-/blob/master/sphinx_petsc.pdf}.
\end{frame}


\section{Motivation}


\begin{frame}[fragile]
\frametitle{Motivation}
While PETSc has a great deal of good documentation, it's not always as easy as it could be to find and interact with:
  \begin{itemize}
    \item PDF manuals for PETSc, TAO, and developers (PDFs) aren't easily usable from the web
    \item Beginners report having a hard time getting up to speed with PETSc
    \begin{itemize}
      \item Not obvious where to start
      \item Not obvious which are the "good" tutorial examples
    \end{itemize}
    \item It's not always easy to edit docs, once you notice they're wrong
    \item The full docs build takes a long time
  \end{itemize}
\end{frame}


\section{Sphinx}


\begin{frame}[fragile]
\frametitle{Sphinx}
  \begin{itemize}
    \item Python module and associated scripts
    \item Based on the \lstinline{docutils} module
    \item Designed for documenting Python itself, on the web (e.g. many \href{https://docs.python.org/3/library/subprocess.html}{pages you've seen like this})
    \item But also used to generate general documentation and websites (e.g. \href{https://scikit-learn.org/stable/}{scikit-learn} (\href{https://github.com/scikit-learn/scikit-learn/tree/master/doc}{source}))
  \end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Sphinx: Pros}
  \begin{itemize}
    \item Designed to produce HTML, LaTeX, and other version of docs
    \item Most content written in a (relatively) easy to edit markup language
    \item Extensible
    \item Widely used (many examples)
    \item Supported by ReadTheDocs
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Sphinx: Cons}
  \begin{itemize}
    \item Dependency on a Python module
    \item Markup syntax (reStructuredText aka reST) has its annoying aspects (though \href{https://www.sphinx-doc.org/en/master/usage/markdown.html}{MarkDown is supported, too})
    \item Easy to bloat with dubious extensions and Python modules
    \item Slower than a wiki
  \end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Learning Sphinx}
  \begin{itemize}
    \item Sphinx has its own \href{}{documentation}, in particular a nice \href{https://www.sphinx-doc.org/en/master/usage/quickstart.html}{quickstart}.
    \item Sphinx includes a very useful \texttt{sphinx-quickstart} script - creating some new docs for your own project is a good way to quickly get some hands-on experience
    \item It's interesting to poke around in other peoples \texttt{conf.py} files (e.g. \href{https://github.com/CEED/libCEED/blob/main/doc/sphinx/source/conf.py}{LibCEED's}).
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Configuration}
  \begin{itemize}
    \item Configuration in a file \texttt{conf.py}
    \item A set of \href{https://www.sphinx-doc.org/en/master/usage/configuration.html}{standard variables} can be manipulated
    \item Executed as Python, so very flexible
    \item Here is \href{https://gitlab.com/petsc/petsc/-/blob/master/src/docs/sphinx_docs/conf.py}{PETSc's version}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\texttt{sphinx-build}}
  \begin{itemize}
    \item \href{https://www.sphinx-doc.org/en/master/man/sphinx-build.html}{\texttt{sphinx-build}} generates documentation from a set of \texttt{.rst} and other files.
    \item The most important argument is is the "builder" \\
      (e.g. \texttt{sphinx-build -b html})
    \item There are also "make modes", for example\\
      \texttt{sphinx-build -M latexpdf}, which uses the \texttt{latex} builder and then calls \texttt{latexmk} for you.
    \item \texttt{sphinx-quickstart} generates a \texttt{Makefile} which wraps \texttt{sphinx-build}
    \item Here is the \href{https://gitlab.com/petsc/petsc/-/blob/master/src/docs/sphinx_docs/Makefile}{The Makefile for PETSc} (doesn't do much, but does highlight cool \href{https://www.sphinx-doc.org/en/master/usage/builders/index.html#sphinx.builders.linkcheck.CheckExternalLinksBuilder}{link-checker})
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Sphinx Basic Usage: reStructuredText}
  \begin{itemize}
    \item Most of the actual content is written in reStructuredText (aka reST aka \texttt{.rst}).
    \item Like Markdown, designed to be simpler and more human-readable than something like HTML
    \item Example: \href{https://docs.petsc.org/en/latest/_sources/developers/documentation.rst.txt}{Developers meta-docs source} \footnote{Note that you can examine the source for any page on ReadTheDocs}
    \item The official docutils descriptions are not always very readable. Start with the \href{https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html}{description in the Sphinx docs}
    \item Quirks and annoyances:
      \begin{itemize}
        \item Indentation matters!
        \item Probably more underscores and colons everwhere than you would like
        \item Annoyance: no nested syntax. This doesn't work: \lstinline{```monospace`` <https://nope.com>`__}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Sphinx Basic Usage: Links}
  \begin{itemize}
    \item External links. I usually use this syntax (two trailing underscores)
      \begin{lstlisting}
        `link text <https://link.target>`__
      \end{lstlisting}
      (Gotcha - the spaces and angle brackets are unforgiving)
    \item Internal links use various roles.
      \begin{itemize}
        \item \lstinline{:doc:`docname`} links to another doc. This is a filename minus the \texttt{.rst}. Path can be relative to the current file or to the Sphinx root directory (leading slash), e.g. \lstinline{:doc:`/manual/index`}
        \item \href{https://www.sphinx-doc.org/en/master/usage/restructuredtext/roles.html#role-ref}{\lstinline{:ref:`my_label`}} refers to an arbitrary point, marked with \lstinline{.. _my_label:}.
        \item Place these labels before section headings to create links with text corresponding to the headings (\href{}{example})
        \item \lstinline{:ref:`custom link text <my_label>`}
      \end{itemize}
    \item Usually you can just use the \href{https://www.sphinx-doc.org/en/master/usage/restructuredtext/roles.html#role-any}{\texttt{:any:} role}, which covers \lstinline{:doc:}, \lstinline{:ref:}, and more.
  \end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Sphinx Basic Usage: Source Listings}
\begin{itemize}
\item Native docutils \href{https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html#literal-blocks}{literal block} way (probably avoid, but Pandoc gives you this)
\begin{lstlisting}
.. highlight:: language

::

    code
\end{lstlisting}
\item Sphinx \href{https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html#directive-code-block}{code block} way
\begin{lstlisting}
.. code-block:: language
    :option:

    code
\end{lstlisting}
\item Sphinx \href{https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html#directive-literalinclude}{literalinclude}. See our \href{https://docs.petsc.org/en/latest/developers/documentation/#sphinx-documentation-guidelines}{notes to make it a little less ornery}.
\end{itemize}
\end{frame}


\section{Sphinx in PETSc}


\begin{frame}[fragile]
\frametitle{HTML docs}
  \begin{itemize}
    \item Some of the docs have been migrated to \url{https://docs.petsc.org}, which is generated by \url{https://readthedocs.org/projects/petsc/}
    \item Starting with the most-important things, the manuals
    \item Offers a pathway to migrate many of the other docs
    \item Not complicated web pages, so we could host them somewhere else if desired (RTD just conveniently runs \texttt{sphinx-build} for us)
    \item There are a couple of \lstinline{.. raw:: html} and \lstinline{.. only:: html} blocks in the code, which should be used sparingly, but are useful when real differences are needed with respect to the non-HTML versions (e.g. embedding a YouTube video)
  \end{itemize}

% Expected to be the primary source of information for most people
% Note that we use the dirhtml builder, which is a subclass of the html builder which makes for cleaner-looking links (locally, it's smoother to just use the html builder)
\end{frame}

\begin{frame}[fragile]
\frametitle{Extensions}
  \begin{itemize}
    \item  Sphinx comes with some extensions, there are many contributed extensions, and you can define your own
    \item PETSc uses \href{https://gitlab.com/petsc/petsc/-/blob/master/src/docs/sphinx_docs/conf.py#L50}{all three types}!
        \begin{itemize}
          \item \texttt{sphinx.ext.graphviz} - \href{https://docs.petsc.org/en/latest/developers/callbacks/}{example} (included)
          \item \texttt{sphinxcontrib.bibtex} - use our existing BibTeX files for references (contributed)
            \begin{itemize}
              \item Currently warns about duplicate citations
              \item \href{https://docs.petsc.org/en/latest/developers/documentation/#bibtex-footnote}{Hoping for a new release including features in their master branch}
              \item You must include \href{https://gitlab.com/petsc/petsc/-/blob/master/src/docs/sphinx_docs/manual/ksp.rst#L2313}{this boilerplate} at the bottom of any \texttt{.rst} files using \texttt{:cite:}.
            \end{itemize}
          \item \texttt{sphinxcontrib.katex} - nicer equation rendering (contributed)
          \item \texttt{sphinxcontrib.rsvgconvertor} - allow \href{https://docs.petsc.org/en/latest/developers/documentation/#sphinx-documentation-guidelines}{SVG images to be used in HTML and LaTeX}.
            \begin{itemize}
              \item Requires you to have the `rsvg-convert` binary.
            \end{itemize}
          \item \texttt{html5\_petsc} (custom)
        \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Custom HTML extension}
  \begin{itemize}
    \item The major development bottleneck was in extending Sphinx to allow automatic links to the PETSc man pages
      \item We use a custom extension which adds links as the reST is converted to HTML, using the same mapping generated for the old LaTeX manual
      \item This depends on \texttt{make allcite}
      \item Thus, the extension will perform a  \href{https://gitlab.com/petsc/petsc/-/blob/master/src/docs/sphinx_docs/ext/html5_petsc.py#L169}{minimal build of PETSc}, if needbe (this is what happens on RTD)
    \item Involves \href{https://gitlab.com/petsc/petsc/-/blob/master/src/docs/sphinx_docs/ext/html5_petsc.py#L85}{overriding functions}, so we \href{https://gitlab.com/petsc/petsc/-/blob/master/src/docs/sphinx_docs/ext/html5_petsc.py#L28}{enforce a strict version of Sphinx} to avoid problems.
  \end{itemize}
\end{frame}


\begin{frame}[fragile]
\frametitle{Sphinx in PETSc: LaTeX/PDF docs}
  \begin{itemize}
    \item To continue having a PDF \texttt{manual.pdf}, we use the \texttt{latexpdf} make mode to generate just that portion of the docs
    \item See \href{https://gitlab.com/petsc/petsc/-/blob/master/src/docs/sphinx_docs/conf.py}{\texttt{conf.py}} for some custom logic to make it into an ANL Tech Report
    \item There are a couple of \lstinline{.. raw:: latex} usages in the manual.
  \end{itemize}

\end{frame}

\begin{frame}[fragile]
  \frametitle{ReadTheDocs (RTD)}
  \begin{itemize}
    \item Operates very similarly to a CI cloud service. Builds your project in a container, when you update your repo, controlled by a \href{https://gitlab.com/petsc/petsc/-/blob/master/.readthedocs.yml}{YAML file}.
    \item \href{https://readthedocs.org/projects/petsc/versions/}{The versions page} on RTD allows version versions corresponding to any git branch or tag.
    \item The default "latest" version currently corresponds to the \texttt{release} branch.
    \item Note that versions can be "hidden".
    \item Gotcha: to allow fast builds, RTD doesn't do a fresh clone. So, sometimes it's necessary to "wipe" a version.\\
      \includegraphics[width=0.4\textwidth]{wipe1} \ \includegraphics[width=0.4\textwidth]{wipe2}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{PETSc in Sphinx: building locally}
There are currently two ways to build the Sphinx docs locally
\begin{itemize}
  \item (Recommended) Use your own Python environment, and work directly in \texttt{src/docs/sphinx\_docs}
    \begin{itemize}
      \item Recall that if needbe our custom extension will actually configure and build a minimal version of PETSc, to allow for the \texttt{htmlmap} file to be created.
    \end{itemize}
  \item Use the targets defined in the \href{}{top-level makefile}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{PETSc Content in Sphinx: developers docs}
  \begin{itemize}
    \item The developers manual has been converted to developers docs, available at \url{https://docs.petsc.org/en/latest/developers}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{PETSc Content in Sphinx: Users Manual}
  \begin{itemize}
    \item The users manual has been converted, available at \url{https://docs.petsc.org/en/latest/manual}
    \item The emphasis was on transferring the existing content to make it visible, not (yet) improving the content.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{PETSc Content in Sphinx: Guide to Tutorials}
  \begin{itemize}
    \item The beginnings of new content are at \url{docs.petsc.org/en/latest/guides/guide_to_examples/}
    \item The aim is to guide users to the most useful of the many tutorial examples
    \item \href{https://docs.petsc.org/en/latest/guides/physics/guide_to_stokes/}{Stokes example from Matt}
    \item Good example of a similar appraoch: \href{https://dealii.org/developer/doxygen/deal.II/Tutorial.html}{deal.ii}
    \item Work still in progress aims at adding content specifically aimed at new users (\href{https://gitlab.com/petsc/petsc/-/issues/330}{Issue 330})
  \end{itemize}
\end{frame}


\section{How to Contribute}


\begin{frame}[fragile]
\frametitle{Contributing for Developers}
  \begin{itemize}
    \item Quickly making small changes:
      \begin{itemize}
        \item Problem: all changes go through Merge Requests, to be rebuilt by ReadTheDocs, which is more-than-ideal overhead for small "wiki-style" edits
        \item Partial solution: use web interfaces as described \href{https://docs.petsc.org/en/latest/developers/documentation/#making-changes-to-the-sphinx-docs-from-the-web}{in the developers meta-docs}
        \item Partial solution: flag your docs-only MR for quick merging, as described \href{https://gitlab.com/petsc/petsc/-/wikis/home#docs-only-changes}{on the developer wiki}\footnote{This content should be migrated to Sphinx!}
      \end{itemize}
    \item For larger contributions, make sure that you can \href{https://docs.petsc.org/en/latest/developers/documentation/#building-the-sphinx-docs-locally}{build the Sphinx docs locally}, with your own Python (and \href{https://www.sphinx-doc.org/en/master/usage/builders/index.html#sphinx.builders.latex.LaTeXBuilder}{LaTeX}, if desired) environment.
    \item To access the PETSc \href{https://readthedocs.org/projects/petsc/}{ReadTheDocs account}, create an account (use your GitLab account/username if possible), and have an existing adminstrator give you access. This will allow you to create a custom version to preview a "real build" with your branch.
  \end{itemize}
\end{frame}


\section{Possible Future Improvements}


\begin{frame}[fragile]
  \frametitle{(Easy) Porting existing docs}
  \begin{itemize}
    \item The front page of the Sphinx docs could easily be expanded to include all the non-generated pages currently on the main PETSc website
    \item \href{https://pandoc.org/demos.html}{Pandoc} can be very helpful for getting a rough first version in reST format, from either HTML or Markdown documents.
    \item The \href{https://gitlab.com/petsc/petsc/-/wikis/home}{developer wiki information} should be moved to the the developers docs section on integration workflows and elsewhere (\href{https://gitlab.com/petsc/petsc/-/issues/596}{Issue 596}). \footnote{Note that if we are really lazy, it's probably possible to just move the MarkDown files over and \href{https://www.sphinx-doc.org/en/master/usage/markdown.html}{have Sphinx render them}}.
    \item The TAO manual can be converted, using the same procedure as the Users Manual (Pandoc, automated fixes, and manual fixes). There is \href{https://docs.petsc.org/en/latest/developers/documentation/#porting-latex-to-sphinx}{temporary documentation in the meta-docs left for this purpose}.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{(Mediium) Integrating man pages and HTML source}
  \begin{itemize}
    \item The man pages, from Sowing, are simply formatted, brittle in some ways, and in need of some cleanup, but are quite functional
    \item Discussions still open on how to best integrate them more fully with the Sphinx docs
    \item A major benefit of this would be that man pages would correspond to the same version of the code as the Sphinx docs (currently all links are to the "release" or "current" version)
    \item An interesting (untested) idea is to include the rendered HTML pages as \href{https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-html_additional_pages}{additional pages to Sphinx}, hopefully allowing it to apply its style sheet and create consistent links.
    \item Similar considerations correspond to the HTML sources from \texttt{c2html}
  \end{itemize}

\end{frame}

\begin{frame}[fragile]
  \frametitle{(Hard) Tutorial Content}
  \begin{itemize}
      \item The aim is a small set of well-maintained introductory tutorials, based on PETSc tutorial examples
      \item Exemplar: \href{https://slepc.upv.es/documentation/}{SLEPc}
      \item Examplar: \href{https://www.dealii.org/current/doxygen/deal.II/Tutorial.html}{deal.ii}
      \item We have had many \href{https://gitlab.com/petsc/petsc/-/issues/330}{discussions already} and have some \href{https://docs.petsc.org/en/wg-beginners-tutorials/tutorials/introductory_tutorial.html}{preliminary ideas and tutorials}, but ...
      \item The hardest task (in my view) because it \textbf{requires coordination and buy-in from those of us who teach (with) PETSc}
  \end{itemize}
\end{frame}

\section{Conclusion}

\begin{frame}[fragile]
\frametitle{Good Documentation}
Good documentation is like a bonsai tree:
\begin{itemize}
  \item Alive
  \item On display
  \item Small
  \item Frequently pruned
  %\item {\tiny And like all physical objects, it only exists in one place}
\end{itemize}
{\small
(Adapted from \href{https://github.com/google/styleguide/blob/gh-pages/docguide/best_practices.md}{the Google style guide})
}

\vspace{10px}

\includegraphics[height=0.25\textwidth]{bonsai.jpg}\\
{\tiny
\href{https://commons.wikimedia.org/wiki/File:California_Juniper_bonsai_220,_October_10,_2008.jpg}{Photo by Sage Ross},
distributed under a \href{https://creativecommons.org/licenses/by-sa/2.5/deed.en}{CC-BY SA 2.5 license}.
}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Summary}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
        \item We're using Sphinx to help make the PETSc docs more accessible and easy to maintain.
        \item      Places to start for information:
          \begin{itemize}
            \item \href{https://www.sphinx-doc.org/en/master/usage/quickstart.html}{Sphinx Quickstart}
            \item PETSc Developers meta-docs (\url{https://docs.petsc.org/en/latest/developers/documentation})
          \end{itemize}
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
    Next steps?
    \begin{itemize}
      \item (Easy) Move more content to Sphinx
        \begin{itemize}
          \item TAO Manual
          \item Website HTML pages
          \item Developer wiki
        \end{itemize}
      \item (Medium) Integrate auto-generated HTML with Sphinx (or replace it)
        \begin{itemize}
          \item man pages
          \item HTML source
        \end{itemize}
      \item (Hard) Tutorials
    \end{itemize}
    \end{column}
  \end{columns}
\end{frame}
\end{document}
